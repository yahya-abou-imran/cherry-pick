dev:
	docker build CherryPick -t cherrypick:local
	nomad agent -dev & (sleep 3 && nomad run infra/dev.nomad)

stop:
	killall nomad
	