job "website" {

  group "website" {
    count = 1

    network {
      port "http-dev" {
        static = "8080"
        to = "80"
      }
    }

    task "website" {
      driver = "docker"

      config {
        image = "cherrypick:local"
        ports = ["http-dev"]
      }

    }
  }
}